/**
 * MemberOrdering
 */
export class MemberOrdering {
    static spub1: string;
    static spub2: number;
    ipub1: string;
    ipub2: number;
    n0: number;
    n1: number;
    n2: number;
    n3: number;
    protected iprot1: string;
    protected iprot2: number;
    private _ipriv1: number;
    private readonly _ipriv2: string;

    constructor(readonly cpub1: number) {}

    get iPriv1(): number {
        return this._ipriv1;
    }
    set iPriv1(v: number) {
        this._ipriv1 = v;
    }

    get iPriv2(): string {
        return this._ipriv2;
    }
}
