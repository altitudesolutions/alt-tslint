import * as Helpers from "./helpers";
/**
 * Documentation must exist for exported classes.
 */
export class ClassInferrableTypes {
    /**
     * no-inferrable-types
     */
    booleanValue = true;
    numberValue: number;
    // tslint:disable-next-line: no-inferrable-types
    stringValue: string = "";

    explicitParam(v: boolean = false): void {
        Helpers.dump(this, v);
    }

    /**
     * Rule ignores params
     */
    explicitParamTwo(v: boolean = false): void {
        Helpers.dump(this, v);
    }
}
