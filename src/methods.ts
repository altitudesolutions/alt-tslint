import * as Helpers from "./helpers";
/**
 * Documentation must exist for exported classes.
 */
export class ClassMethods {
    invokeUnused(): void {
        this.noEmptyPrivate();
    }
    // tslint:disable-next-line: no-empty prefer-function-over-method
    noEmptyPublic(): void {}
    // tslint:disable-next-line: prefer-function-over-method
    preferFunctionPublic(): void {
        Helpers.dump(this);
    }

    // tslint:disable-next-line: no-empty
    protected noEmptyProtected(): void {}

    protected preferFunctionProtected(): void {
        Helpers.dump();
    }
    // tslint:disable-next-line: no-empty prefer-function-over-method
    private noEmptyPrivate(): void {}

    // tslint:disable-next-line: prefer-function-over-method
    private preferFunctionPrivate(): void {
        Helpers.dump();
    }
}
