import * as Helpers from "./helpers";

export function strictBooleanOK(cmp: any): boolean {
    return cmp != undefined;
}

export function strictBooleanNotOK(cmp: number): boolean {
    let result = false;
    // tslint:disable-next-line: strict-boolean-expressions
    if (cmp) {
        result = true;
    }

    return result;
}

export function curly(n: number): boolean {
    // Rule: curly ignore-same-line
    if (n == undefined) return false;
    if (n === 1) {
        Helpers.dump(n);
    }

    return true;
}

export function forin(o: { [key: string]: string }): void {
    for (const k in o) {
        // Rule: forin
        if (!o.hasOwnProperty(k)) continue;
        Helpers.dump(k);
    }
}

export function noforin(o: string[]): void {
    if (o == undefined) throw new Error();
    // Rule: no-for-in
    // tslint:disable-next-line: no-for-in-array
    for (const k in o) {
        if (!o.hasOwnProperty(k)) continue;
        Helpers.dump(k);
    }

    // Correct
    for (const k of o) {
        Helpers.dump(k);
    }
}

export function noempty(n: number): void {
    // tslint:disable-next-line: no-empty
    if (n > 0) {
    }
    try {
        Helpers.dump(n);
        // tslint:disable-next-line: no-empty
    } catch {}
}

// tslint:disable-next-line: no-empty
export function noemptyFn(): void {}

export function noVoidExpression(): void {
    // tslint:disable-next-line: no-void-expression
    Helpers.dump(noemptyFn());
    Helpers.dump(noemptyFn);
}

export class NoVoidExpressionHelper {
    noVoidExpression(): void {
        // "ignore-arrow-function-shorthand" allows this style
        Helpers.dump(() => this.returnsVoid());
    }
    returnsVoid(): void {
        Helpers.dump(this);
    }
}

export function arrowParens(): void {
    const a = [0, 1].map(v => v.toString());
    Helpers.dump(a);
    const b = [0, 1].map((v, i) => {
        Helpers.dump(i);

        return v.toString();
    });
    Helpers.dump(a, b);
}

// The maximum line length is set to 120 characters.
// tslint:disable-next-line: max-line-length
// 012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789 This line is too long!

export function typeAssertion(): void {
    // We allow angle-bracket type assertion - this is proper Typescript syntax.
    const s1: string = Helpers.argOfType(<string>(<unknown>undefined));
    // The `as` operator was introduced because  angle-bracket type assertion was syntactical incompatibility with JSX,
    // but both have precisely the same meaning.
    const s2: string = Helpers.argOfType((<unknown>undefined) as string);
    Helpers.dump(s1, s2);
}

export function strictStringExpressions(n: number): void {
    const barFunc = () => "World";
    const fooStr = `Hello, ${barFunc}`; // Hello, World
    // tslint:disable-next-line: prefer-template restrict-plus-operands
    const s = "Number " + n;
    Helpers.dump(s);
}

export function parameterReassignment(p1: number): void {
    // Should allow parameter reassignment.
    if (p1 < 0) {
        p1 = 0;
    }
    Helpers.dump(p1);
}
