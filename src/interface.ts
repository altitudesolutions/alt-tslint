/**
 * Allow empty interfaces (useful for inherited interfaces which are expected to be implemented in a future release).
 */
export interface IA {}
