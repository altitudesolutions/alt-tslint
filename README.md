# Altitude tslint rules

These rules are intended for standalone Typescript library projects, and not for use when developing with frameworks such as Angular.

## Config

For VSCOde, the [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) and [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin) extensions should be installed.

For a typical Typescript library project, we configure tslint as follows (all files in the project root directory):

### package.json

We need to add `tslint-config-prettier` as a dev dependency:

```ps1
npm i --save-dev tslint-config-prettier
```

### tslint.json

We include `tslint-config-prettier` as the final element in the `extends` array so that it cancels any rules which conflict with its own.

```json
{
    "extends": ["@altitude/tslint", "tslint-config-prettier"]
}
```

## Notes

https://palantir.github.io/tslint/rules/

https://github.com/palantir/tslint/blob/master/src/configs/all.ts

### "strict-type-predicates": false

Clashes with triple-equals, does not allow null-parameter guards for required params.

### "no-boolean-literal-compare": false

We typically want to ensure that a boolean (and not some truthy value) has been passed when evaluating parameters from client code
which may not enforce strict type safety rules (eg this.disabled = disabledParam === true).
